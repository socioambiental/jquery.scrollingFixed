/**
 * Smooth scrolling with fixed top menu.
 */
(function ($) {
  $.fn.scrollingFixed = function(options) {
    var self               = this;
    var firstRun           = true;
    var preventScroll      = false;
    var currentId          = null;
    var pageTitle          = $(document).prop('title');
    var settings           = $.extend({
      // We're using different fixed menu offsets for when user scroll the window
      // or click in the menu bar
      menuOffsetScroll: '50',
      menuOffsetClick:  '20',
      currentMenuItem:  '#currentMenuItem',
      menu:             '#scrollmenu',
      menuSymbol:       '&#9776;',
    }, options );

    // Set initial anchor
    if (window.location.hash.replace(/^(.).*/, "$1") == '#') {
      var hash = window.location.hash;

      // Initial activation
      activate(hash, true);

      // Final activation to fix vertical position
      $(window).load(function() {
        // Only scroll to initial hash if user hasn't scrolled the page
        if (window.location.hash == hash) {
          activate(hash, true);
        }
        else {
          // See http://stackoverflow.com/questions/19662018/console-log-not-working-at-all
          $(window).scroll();
        }
      });
    }

    // This handles if user changes section using the browser address bar
    $(window).bind('hashchange', function(e) {
      activate(window.location.hash, true, false);
    });

    /**
     * This part handles scrolling.
     */
    $("a").click(function(evn) {
      var theId = $(this).attr('href');

      // Process local links, ie, only anchors (#) in the current page
      if (theId.replace(/^(.).*/, "$1") != '#') {
        return;
      }

      // Smooth scrolling using scrollto.js
      // Does not work well with our activate() function
      //evn.preventDefault();
      //$('html,body').scrollTo(theId, theId);

      // Activate the chosen section
      evn.preventDefault();
      activate(theId, true);
    });

    /**
     * This part handles the highlighting functionality.
     * We use the scroll functionality again, some array creation and
     * manipulation, class adding and class removing, and conditional testing
     */
    var aChildren = $(settings.menu + " li").children(); // find the a children of the list items
    var aArray    = [];                             // create the empty aArray

    for (var i=0; i < aChildren.length; i++) {
      var aChild = aChildren[i];
      var ahref  = $(aChild).attr('href');

      aArray.push(ahref);
    } // this for loop fills the aArray with attribute href values

    $(window).scroll(function() {
      var windowPos      = $(window).scrollTop(); // get the offset of the window from the top of page
      var windowHeight   = $(window).height();    // get the height of the window
      var docHeight      = $(document).height();
      var menuHeight     = $(self).height();

      if (firstRun == true) {
        firstRun = false;
        return;
      }

      if (preventScroll == true) {
        preventScroll = false;
        return;
      }

      for (var i=0; i < aArray.length; i++) {
        var theId      = aArray[i];
        var divPos     = $(theId).offset().top; // get the offset of the div from the top of page
        var divHeight  = $(theId).height();     // get the height of the div in question
        var upperLimit = divPos - menuHeight - settings.menuOffsetScroll;
        var lowerLimit = upperLimit + divHeight;

        if (windowPos >= upperLimit && windowPos < lowerLimit) {
          activate(theId);
        } else {
          deactivate(theId);
        }
      }

      // Border conditions
      if (windowPos <= menuHeight) {
        var navActiveCurrent = $(".nav-active").attr("href");

        deactivate(navActiveCurrent);
        activate();
      }

      if (windowPos + windowHeight == docHeight) {
        if (!$(settings.menu + " li:last-child a").hasClass("nav-active")) {
          var navActiveCurrent = $(".nav-active").attr("href");
          var item             = $(settings.menu + " li:last-child a");
          var id               = item.attr('href');

          deactivate(navActiveCurrent);
          activate(id);
        }
      }
    });

    /**
     * Activate a section.
     */
    function activate(theId, scroll, hashchange) {
      // Default Id
      if (theId == '' || theId === undefined || theId == null) {
        theId = '#' + $(self).attr('id');
      }

      var title      = $('[href=' + theId + ']').html();
      var windowPos  = $(window).scrollTop(); // get the offset of the window from the top of page
      var menuHeight = $(self).height();

      if (scroll == true) {
        var divPos     = $(theId).offset().top; // get the offset of the div from the top of page
        var upperLimit = divPos - menuHeight - settings.menuOffsetClick;
        preventScroll  = true;

        // Adjust position taking the fixed menu bar into account
        $(window).scrollTop(upperLimit);
      }

      // Check if section is currently active
      if (currentId == theId) {
        return;
      }
      else {
        currentId = theId;
      }

      $("a[href='" + theId + "']").addClass("nav-active");
      $(settings.currentMenuItem).html(settings.menuSymbol + '&nbsp;' + title);
      $(document).prop('title', pageTitle + ' - ' + title);

      /**
       * Set window location only if needed, otherwise the browser will keep
       * moving to the top of the section and effectivelly blocking scrolling.
       */
      if (window.location.hash != theId && hashchange != false) {
        // Window location version
        //preventScroll        = true;
        //window.location.hash = theId;

        // Browser history version, which doesn't trigger a scroll event
        history.pushState({}, "", theId);
      }
    }

    /**
     * Deactivate a section.
     */
    function deactivate(theId) {
      $("a[href='" + theId + "']").removeClass("nav-active");
    }

    return this;
  };
}(jQuery));
